using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

class MyTcpClient
{
	public static void Main(String[] args)
	{
		string message;
		byte[] data;
		// Create a TcpClient.
		// Note, for this client to work you need to have a TcpServer
		// connected to the same address as specified by the server, port
		// combination
		Int32 port = 13000;
		TcpClient client = new TcpClient("127.0.0.1", port);

		try
		{
			// Get a client stream for reading and writing
			// Stream stream = client.GetStream();
		
			NetworkStream stream = client.GetStream();
			Console.WriteLine("Connected to server");


			while(true)
			{
				Console.WriteLine("Format : number operator:*,/,+,- number\nSeparated by space\n Input : ");
				message = Console.ReadLine();
				// Translate the passed message into ASCII and store it as a Byte array
				data = System.Text.Encoding.ASCII.GetBytes(message);

				// Send the message to the connected TcpServer
				stream.Write(data, 0, data.Length);

				Console.WriteLine("Sent : {0}", message);

				// Receive the Tcp Server response

				// Buffer to store the response bytes
				data = new Byte[256];

				// String to store the response ASCII representation
				String responseData = String.Empty;

				// Read the first batch of the TcpServer response bytes.
				Int32 bytes = stream.Read(data, 0, data.Length);
				responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
				Console.WriteLine("Received: {0}", responseData);
			}

		}
		catch (ArgumentNullException e)
		{
			Console.WriteLine("ArgumentNullException : {0}", e);
		}

		Console.WriteLine("\n Press Enter to continue...");
		Console.Read();
	}
}
