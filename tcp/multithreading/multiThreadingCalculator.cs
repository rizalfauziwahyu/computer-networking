using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ConsoleApplication1
{
	class MyTcpSetUpConnection
	{

		private static void Main(String[] args)
		{
			TcpListener server = null;
			bool waitingConnection = true;
			int counter = 0;
			TcpClient client;

			try
			{
				// Set the Tcp Listener on port 13000
				Int32 port = 13000;

				// TcpListener server = new TcpListener(port);
				server = new TcpListener(IPAddress.Any, port);

				// Start listening for client request
				server.Start();
				
				// Start Client
				while (waitingConnection)
				{
					try 
					{
						counter++;
						Console.WriteLine("Waiting for a connection.. ");

						// Perform a blocking call to accept request

						client = server.AcceptTcpClient();
						Console.WriteLine(" >> Client No : {0} started", counter);

						ProcessCalculation processToThread = new ProcessCalculation();
						processToThread.startClient(client, Convert.ToString(counter));
					}
					catch (Exception e)
					{
						Console.WriteLine(e.ToString());
						waitingConnection = false;
					}

				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				server.Stop();
			}
		}
	}

	class ProcessCalculation
	{
		TcpClient clientSocket;
		string clNo;
		public void startClient(TcpClient inClientSocket, string clientNumber)
		{
			this.clientSocket = inClientSocket;
			this.clNo = clientNumber;

			Thread ctThread = new Thread(gettingParameter);
			ctThread.Start();
		}
		private void gettingParameter()
		{
			byte[] bytesFrom = new byte[100000025];
			string dataFromClient = null;
			byte[] sendBytes = null;
			string serverResponse = null;
			float finalResult;
			NetworkStream stream = null;
			bool waitingForStream = true;

			while (waitingForStream)
			{
				try {
					// Get a stream object for reading and writing
					stream = clientSocket.GetStream();
					stream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
					dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
					Console.WriteLine("Getting Parameter From client -" + this.clNo + " : " + dataFromClient);
					finalResult = Calculator(dataFromClient);
					Console.WriteLine("Calculation result for client -" + this.clNo + " is " + Convert.ToString(finalResult));
					serverResponse = "result is " + Convert.ToString(finalResult);
					Console.WriteLine("result is : {0}", finalResult);
					sendBytes = System.Text.Encoding.ASCII.GetBytes(serverResponse);
					stream.Write(sendBytes, 0, sendBytes.Length);
				}
				catch (Exception e)
				{
					stream.Close();
					clientSocket.Close();
					waitingForStream = false;
					this.clNo = null;
					this.clientSocket = null;
					Console.WriteLine(e.ToString());
				}
			}
		}

		private float Calculator(string argumentForCalculation)
		{
			string argument = argumentForCalculation;
			float numberOne;
			float numberTwo;
			char operand;
			float result = 0;
			// Argument separated by space
			string[] splitArgument = argument.Split(" ");
			
			numberOne = Convert.ToSingle(splitArgument[0]);
			operand = char.Parse(splitArgument[1]);
			numberTwo = Convert.ToSingle(splitArgument[0]);
			
			switch(operand)
			{
				case '+':
						result = numberOne + numberTwo;
						break;
				case '-':
						result = numberOne - numberTwo;
						break;
				case '*':
						result = numberOne * numberTwo;
						break;
				case '/':
						result = numberOne / numberTwo;
						break;
			}
			
			return result;
		}
	}

}
