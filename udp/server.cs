using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UDPServer 
{
	private Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
	private const int bufSize = 8 * 1024;
	private State state = new State();
	private EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
	private AsyncCallback recv = null;
	private string serverName = "Devrafaezya";
	private string client;
	private string message;

	public class State
	{
		public byte[] buffer = new byte[bufSize];
	}

	public void Server(string address, int port)
	{
		_socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
		_socket.Bind(new IPEndPoint(IPAddress.Parse(address), port));
		Receive();
	}

	public void Receive()
	{
		_socket.BeginReceiveFrom(state.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv = (ar) => {
			State so = (State)ar.AsyncState;
			int bytes = _socket.EndReceiveFrom(ar, ref epFrom);
			_socket.BeginReceiveFrom(so.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv, so);
			if (String.IsNullOrEmpty(client)) {
				client = Encoding.ASCII.GetString(so.buffer, 0, bytes);
			}
			Console.WriteLine("{0} RECV: {1}, data size = {2}, data = {3}", serverName, epFrom.ToString(), bytes, Encoding.ASCII.GetString(so.buffer, 0, bytes));
		}, state);
	}

	public void Send(string text, string address, int port)
	{
		_socket.Connect(IPAddress.Parse(address), port);
		byte[] data = Encoding.ASCII.GetBytes(text);
		_socket.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) => {
			State so = (State)ar.AsyncState;
			int bytes = _socket.EndSend(ar);
			Console.WriteLine("SEND to {0} : {1}, size data = {2}", client, text, bytes);
		}, state);
	}

	static void Main(string[] args)
	{
		UDPServer s = new UDPServer();
		s.Server("127.0.0.1", 8080);
		do{			
			Console.WriteLine("Send your message : ");
			s.message = Console.ReadLine();
			s.Send(s.message, "127.0.0.1", 8080);
		} while (true);
	}
}
