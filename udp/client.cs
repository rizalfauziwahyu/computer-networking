using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UDPClient
{
	private Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
	private const int bufSize = 8 * 1024;
	private State state = new State();
	private EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
	private AsyncCallback recv = null;
	private string clientName = "Fauzi";
	private string serverName;
	private string message;

	public class State
	{
		public byte[] buffer = new byte[bufSize];
	}

	public void Client(string address, int port)
	{
		_socket.Connect(IPAddress.Parse(address), port);
		Receive();
	}

	public void Receive()
	{
		_socket.BeginReceiveFrom(state.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv = (ar) => {
				State so = (State)ar.AsyncState;
				int bytes = _socket.EndReceiveFrom(ar, ref epFrom);
				_socket.BeginReceiveFrom(so.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv, so);
				if(String.IsNullOrEmpty(serverName)) {
						serverName = Encoding.ASCII.GetString(so.buffer, 0, bytes);
				}
				Console.WriteLine("{0} RECV: {1}, data size = {2}, data = {3}", clientName, epFrom.ToString(), bytes, Encoding.ASCII.GetString(so.buffer, 0, bytes));
			}, state);
	}

	public void Send(string text)
	{
		byte[] data = Encoding.ASCII.GetBytes(text);
		_socket.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) => {
				State so = (State)ar.AsyncState;
				int bytes = _socket.EndSend(ar);
				Console.WriteLine("SEND to {0} : {1}, size data = {2}", serverName, text, bytes);		
			}, state);
	}

	static void Main(string[] args)
	{
		UDPClient c = new UDPClient();
		c.Client("127.0.0.1", 8080);
		do{	
			Console.WriteLine("Send your message : ");
			c.message = Console.ReadLine();
			c.Send(c.message);
		} while (true);
	}
}

	
